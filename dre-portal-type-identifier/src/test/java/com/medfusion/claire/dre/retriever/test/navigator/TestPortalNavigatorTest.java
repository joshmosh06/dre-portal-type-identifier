package com.medfusion.claire.dre.retriever.test.navigator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.medfusion.claire.app.data.DreStatusType;
import com.medfusion.claire.app.data.TestParams;
import com.medfusion.claire.app.data.credential.CredentialInfo;
import com.medfusion.claire.app.data.credential.TestCredentialInfo;
import com.medfusion.claire.app.data.ticket.DreTicketInfo;
import com.medfusion.claire.dre.credentials.CredentialFetcher;
import com.medfusion.claire.dre.retriever.navigator.BaseNavigator;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;

import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class TestPortalNavigatorTest {
	@Tested(availableDuringSetup = true)
	private PortalTypeIdentifierNavigator testNavigator;
	
	@Mocked
	private BaseNavigator mockBaseNavigator;
	
	@Mocked
	private TestCredentialInfo mockCredentialInfo;
	
	@Mocked
	private TestParams mockTestParams;
	
	@Mocked
	private CredentialFetcher mockCredentialFetcher;
	
	@Mocked
	private DreTicketInfo mockTicket;

	@Test
	public void testGetStrings() {
		// method under test
		assertNull(this.testNavigator.getStrings());
	}
	
	@Test
	public void testGetDownloader() {
		// method under test
		assertNull(this.testNavigator.getDownloader());
	}

	
	@Test
	public void testLogin() {		
		new Expectations() {
			{
				TestPortalNavigatorTest.this.mockBaseNavigator.getTicket();
				this.result = TestPortalNavigatorTest.this.mockTicket;
				
				TestPortalNavigatorTest.this.mockTicket.getTicketId();
				this.result = 1L;
				
				TestPortalNavigatorTest.this.mockBaseNavigator.getCredentialFetcher();
				this.result = TestPortalNavigatorTest.this.mockCredentialFetcher;
				
				TestPortalNavigatorTest.this.mockCredentialFetcher.getCredentials(this.anyLong);
				this.result = TestPortalNavigatorTest.this.mockCredentialInfo;
				
				TestPortalNavigatorTest.this.mockTestParams.getForcedReturnStatus();
				this.result = DreStatusType.SUCCESS;
			}
		};
		// method under test
		assertEquals(DreStatusType.SUCCESS, this.testNavigator.login(this.mockCredentialInfo).getLeft());
	}
	
	@Test
	public void testLogin_errorUserAuth() {		
		new Expectations() {
			{
				TestPortalNavigatorTest.this.mockBaseNavigator.getTicket();
				this.result = TestPortalNavigatorTest.this.mockTicket;
				
				TestPortalNavigatorTest.this.mockTicket.getTicketId();
				this.result = 1L;
				
				TestPortalNavigatorTest.this.mockBaseNavigator.getCredentialFetcher();
				this.result = TestPortalNavigatorTest.this.mockCredentialFetcher;
				
				TestPortalNavigatorTest.this.mockCredentialFetcher.getCredentials(this.anyLong);
				this.result = TestPortalNavigatorTest.this.mockCredentialInfo;
				
				TestPortalNavigatorTest.this.mockTestParams.getForcedReturnStatus();
				this.result = DreStatusType.ERROR_USER_AUTH;
			}
		};
		// method under test
		assertEquals(DreStatusType.ERROR_USER_AUTH, this.testNavigator.login(this.mockCredentialInfo).getLeft());
	}
	
	@Test
	public void testLogout() {
		// method under test
		this.testNavigator.logout();
	}
	
	@Test
	public void testGoHome() {
		// method under test
		this.testNavigator.goHome();
	}
}
