package com.medfusion.claire.dre.retriever.test.strings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class TestPortalPortalStringsTest {
	private PortalTypeIdentifierStrings testStrings;

	@Before
	public void setUp() throws Exception {
		this.testStrings = new PortalTypeIdentifierStrings();
	}

	@Test
	public void testGetDocumentsList() throws Exception {
		assertNotNull(this.testStrings.getDocumentsList());
	}

	@Test
	public void testGetDocumentsPage() throws Exception {
		assertNotNull(this.testStrings.getDocumentsPage());
	}

	@Test
	public void testSetDocumentsList() throws Exception {
		assertNotNull(this.testStrings.getDocumentsList());
		this.testStrings.setDocumentsList("");
		assertEquals("", this.testStrings.getDocumentsList());
		this.testStrings.setDocumentsList("override");
		assertEquals("override", this.testStrings.getDocumentsList());
		this.testStrings.setDocumentsList(null);
		assertEquals("override", this.testStrings.getDocumentsList());
	}

	@Test
	public void testSetDocumentsPage() throws Exception {
		assertNotNull(this.testStrings.getDocumentsPage());
		this.testStrings.setDocumentsPage("");
		assertEquals("", this.testStrings.getDocumentsPage());
		this.testStrings.setDocumentsPage("override");
		assertEquals("override", this.testStrings.getDocumentsPage());
		this.testStrings.setDocumentsPage(null);
		assertEquals("override", this.testStrings.getDocumentsPage());
	}

}
