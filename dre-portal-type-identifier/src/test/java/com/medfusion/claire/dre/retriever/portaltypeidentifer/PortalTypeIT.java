package com.medfusion.claire.dre.retriever.portaltypeidentifer;

import org.junit.runner.RunWith;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import com.medfusion.claire.app.data.PortalInfo;
import com.medfusion.claire.app.data.ticket.DreTicketInfo;
import com.medfusion.claire.dre.retriever.data.PortalTypeIdentifyTask;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.PortalTypeRetriever;
import com.medfusion.claire.dre.retriever.test.BaseRetrieverIT;

import mockit.integration.junit4.JMockit;

/**
 * PortalTypeIdenitifer IT. Adjust tasks as needed in the {@link #updateTicket} method.
 * Mandatory sysprops:
 * -Dcom.medfusion.claire.dre.scrapers.portaltypeidenitifer.url=
 * Optional sysprops:
 * -Dcom.medfusion.claire.dre.baseDataDir="./target/baseDreDataDir"
 */
@RunWith(JMockit.class)
public class PortalTypeIT extends BaseRetrieverIT {
	
	private static final Logger LOG = LoggerFactory.getLogger(PortalTypeIT.class);
	
	public PortalTypeIT() {
		super("portaltypeidenitifer", new PortalTypeRetriever());
	}

	@Override
	protected void updateTicket(DreTicketInfo ticket) {
//		String URL = "https://www.dukemychart.org/home/";
//		String name = "Test Portal";
//		
//		PortalInfo info = new PortalInfo();
//		info.setPrimaryUrl(URL);
//		info.setName(name);
//		ticket.setPortalInfo(info);
		ticket.addTask(new PortalTypeIdentifyTask());
		
	}

}
