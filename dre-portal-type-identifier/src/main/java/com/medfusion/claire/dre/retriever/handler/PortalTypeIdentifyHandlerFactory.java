package com.medfusion.claire.dre.retriever.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.medfusion.claire.dre.retriever.navigator.BaseNavigator;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;

public class PortalTypeIdentifyHandlerFactory extends BaseTaskHandlerFactory {
	private static final Logger LOG = LoggerFactory.getLogger(PortalTypeIdentifyHandlerFactory.class);

	@Override
	public BaseHandler createPortalTypeIdentifyTaskHandler(BaseNavigator navigator) {
		return new BasePortalTypeIdentifyHandler((PortalTypeIdentifierNavigator) navigator);
	}
}
