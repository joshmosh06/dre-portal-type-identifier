package com.medfusion.claire.dre.retriever.portaltypeidentifiers;

import java.util.ArrayList;
import java.util.Arrays;

import com.medfusion.claire.dre.retriever.portaltypeidentifier.data.PortalTypeIdentifier;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;

public class Epic2017PortalTypeIdentifier extends PortalTypeIdentifier  {
	
	private static final ArrayList<String> EPIC2017_PAGE_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "mychart", "epic" }));

	private static final ArrayList<String> EPIC2017_URL_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "mychart" }));
	
	public Epic2017PortalTypeIdentifier(PortalTypeIdentifierNavigator navigator) {
		super(navigator, EPIC2017_PAGE_KEYWORDS, EPIC2017_URL_KEYWORDS);
	}

	@Override
	public String getPortalType() {
		return "epic2017";
	}

}
