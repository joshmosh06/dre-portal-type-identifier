package com.medfusion.claire.dre.retriever.portaltypeidentifier;


import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.medfusion.claire.app.data.ticket.DreTicketInfo;
import com.medfusion.claire.app.data.ticket.TaskResultInfo;
import com.medfusion.claire.app.data.ticket.TaskResultType;
import com.medfusion.claire.app.data.ticket.TicketTaskInfo;
import com.medfusion.claire.dre.clients.DirectClient;
import com.medfusion.claire.dre.clients.SeleniumClient;
import com.medfusion.claire.dre.exceptions.NonRecoverableException;
import com.medfusion.claire.dre.exceptions.TicketRetryException;
import com.medfusion.claire.dre.retriever.BaseRetriever;
import com.medfusion.claire.dre.retriever.handler.PortalTypeIdentifyHandlerFactory;
import com.medfusion.claire.dre.retriever.navigator.BaseNavigator;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;
import com.medfusion.claire.dre.retriever.strings.PortalStrings;
import com.medfusion.claire.dre.retriever.strings.PortalStringsConfigurator;
import com.medfusion.claire.dre.retriever.test.strings.PortalTypeIdentifierStrings;
import com.medfusion.claire.dre.retriever.ticket.RetrieverTicketLifecycle;
import com.medfusion.claire.dre.utils.TicketUtils;

public class PortalTypeRetriever extends BaseRetriever {
    private static final Logger LOG = LoggerFactory.getLogger(PortalTypeRetriever.class);
    
    /**
     * Shared among retrievals.
     */
    protected RetrieverTicketLifecycle lifecycle;
    
    /**
     * Initialized per-retrieval.
     */
    protected SeleniumClient seleniumClient;
    protected DirectClient directClient;
    protected BaseNavigator navigator;
    
	private PortalStringsConfigurator<PortalStrings> configurator;
    
	public PortalTypeRetriever() {
		navigator = new PortalTypeIdentifierNavigator(false);
		final PortalTypeIdentifyHandlerFactory handlerFactory = new PortalTypeIdentifyHandlerFactory();
		lifecycle = new RetrieverTicketLifecycle((PortalTypeIdentifierNavigator) navigator, handlerFactory);
		configurator = new PortalStringsConfigurator<>();
	}

	@Override
	public void processTicket(DreTicketInfo ticket) {
		LOG.debug("processTicket() - called with ticketId = {}", ticket.getTicketId());

		final PortalStrings strings = constructPortalStrings(ticket);
		initalize(ticket, strings);
		
		try {
			LOG.info("processTicket() - Starting type Identification");
	
			ticket.getTasks().stream().forEach(this::executeStandardTask);
			
		} catch (final Exception e) {
			// any Exception that gets to this point is expected to be handled by the Camel
			// error handlers
			LOG.error("processTicket() - unexpected error, aborting ticket: " + e.getMessage());
			throw e;
		} finally {
			lifecycle.close();
		}
	}

	private void initalize(DreTicketInfo ticket, final PortalStrings strings) {
		this.navigator.setTicket(ticket);
		this.navigator.setStrings(strings);
		this.navigator.getTicket().setJobStatus(null);
		this.navigator.getTicket().getTasks().stream().forEach(t -> t.setResults(new ArrayList<>()));
		if (null == ticket.getTicketId()) {
			throw new NonRecoverableException("ticketId cannot be null");
		}
		if (null == this.navigator.getTicket().getPortalInfo()) {
			throw new NonRecoverableException("Portal Info cannot be null");
		}
		if (StringUtils.isBlank(this.navigator.getTicket().getPortalInfo().getPrimaryUrl())) {
			throw new NonRecoverableException("Primary Url cannot be empty");
		}
		TicketUtils.resolveAndCreateTicketFolders(this.navigator.getTicket());
	}
	
	  /**
     * Catches all Exceptions and fails the associated task. Rethrows only NonRecoverableExceptions and
     * TicketRetryExceptions. A standard task is one in which failure should not halt execution of the overall ticket.
     * Note that NonRecoverableExceptions and TicketRetryExceptions are the only way to halt ticket execution from
     * within a standard task.
     *
     * @param task
     */
    protected void executeStandardTask(final TicketTaskInfo task) {
        try {
            lifecycle.handleTask(task);
        } catch (final WebDriverException e) {
            failStandardTask(task, e);
        } catch (NonRecoverableException | TicketRetryException e) {
            failStandardTask(task, e);
            throw e;
        } catch (final Exception e) {
            failStandardTask(task, e);
        }
    }

    private void failStandardTask(final TicketTaskInfo task, final Exception e) {
        LOG.error("failStandardTask() - unexpected task failure, failing task: " + e.getMessage(), e);
        task.addResult(new TaskResultInfo(TaskResultType.FAILED, null, null));
    }

	@Override
	public PortalStrings constructPortalStrings(DreTicketInfo ticket) {
		final Map<String, String> overrides = ticket.getPortalInfo().getPortalOverrides();
		final PortalTypeIdentifierStrings defaults = new PortalTypeIdentifierStrings();
		return this.configurator.mergePortalOverrides(defaults, overrides);
	}

	@Override
	public BaseNavigator getNavigator() {
		return navigator;
	}

}
