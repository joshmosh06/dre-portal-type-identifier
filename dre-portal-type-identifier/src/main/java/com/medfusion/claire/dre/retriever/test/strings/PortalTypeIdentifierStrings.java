package com.medfusion.claire.dre.retriever.test.strings;

import java.util.ArrayList;
import java.util.Arrays;

import com.medfusion.claire.dre.retriever.strings.PortalStrings;

/**
 * Overridable PortalStrings for the test portal.
 */
@SuppressWarnings("unused")
public class PortalTypeIdentifierStrings extends PortalStrings {

	private static final ArrayList<String> EPIC2017_PAGE_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "mychart", "epic"}));
	
	private static final ArrayList<String> EPIC2017_URL_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "mychart"}));
	
	private static final String DOCUMENTS_LIST = "//table/tbody/tr/td/form/button";

	private static final String DOCUMENTS_PAGE = "https://www.easypacelearning.com/english-books/english-books-for-download-pdf/category/6-english-grammar-pdf-and-word-doc";
	
	private ArrayList<String> epic2017PageKeywords;

	private String documentsList = DOCUMENTS_LIST;

	private String documentsPage = DOCUMENTS_PAGE;
	
	public PortalTypeIdentifierStrings() {
		super();
		
	}


	/**
	 * Default value: {@value #DOCUMENTS_LIST}
	 */
	public String getDocumentsList() {
		return this.documentsList;
	}

	public void setDocumentsList(final String documentsList) {
		this.documentsList = this.trimNonNullOrDefault(documentsList, this.documentsList);
	}

	/**
	 * Default value: {@value #DOCUMENTS_PAGE}
	 */
	public String getDocumentsPage() {
		return this.documentsPage;
	}

	public void setDocumentsPage(final String documentsPage) {
		this.documentsPage = this.trimNonNullOrDefault(documentsPage, this.documentsPage);
	}
}
