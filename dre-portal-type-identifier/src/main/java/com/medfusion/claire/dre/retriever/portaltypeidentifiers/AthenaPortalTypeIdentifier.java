package com.medfusion.claire.dre.retriever.portaltypeidentifiers;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.By;

import com.medfusion.claire.dre.retriever.portaltypeidentifier.data.PortalTypeIdentifier;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;

public class AthenaPortalTypeIdentifier extends PortalTypeIdentifier  {
	
	private static final ArrayList<String> ATHENA_PAGE_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "athena" }));

	private static final ArrayList<String> ATHENA_URL_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "athena", "athenahealth" }));
	
	private static final String POWEREDBY_IMG = "//*[@id='footer-athena']";
	
	public AthenaPortalTypeIdentifier(PortalTypeIdentifierNavigator navigator) {
		super(navigator, ATHENA_PAGE_KEYWORDS, ATHENA_URL_KEYWORDS);
	}
	
	@Override
	public boolean isUniquePortalInfoCorrect() {
		return getNavigator().getSeleniumClient().xpathElementIsPresent(By.xpath(POWEREDBY_IMG));
	}

	@Override
	public String getPortalType() {
		return "athena";
	}
}