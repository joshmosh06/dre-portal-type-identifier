package com.medfusion.claire.dre.retriever.portaltypeidentifier.data;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;

public abstract class PortalTypeIdentifier {
	private static final Logger LOG = LoggerFactory.getLogger(PortalTypeIdentifier.class);
	private PortalTypeIdentifierNavigator navigator;
	//all keywords will be compared to the screen in lowercase
	private ArrayList<String> pageKeywords;
	private ArrayList<String> urlKeywords;
	
	public PortalTypeIdentifier(PortalTypeIdentifierNavigator navigator, ArrayList<String> pageKeywords,
			ArrayList<String> urlKeywords) {
		this.navigator = navigator;
		this.pageKeywords = pageKeywords;
		this.urlKeywords = urlKeywords;
	}
	
	public boolean isCopyrightCorrectType() {
		return navigator.isCopyrightPresent(pageKeywords);
	}
	
	public boolean isUniquePortalInfoCorrect() {
		//Override this method to add unique functionality
		return false;
	}
	
	public boolean isURLCorrect() {
		return navigator.isURLCorrect(urlKeywords);
	}
	
	public abstract String getPortalType();
	
	public void close() {
		LOG.debug("close() - called with ticketId = {}",
				null == this.navigator.getTicket() ? null : this.navigator.getTicket().getTicketId());

		if (null != this.navigator.getSeleniumClient()) {
			this.navigator.getSeleniumClient().close();
		}
		if (null != this.navigator.getDirectClient()) {
			this.navigator.getDirectClient().close();
		}
	}

	public PortalTypeIdentifierNavigator getNavigator() {
		return this.navigator;
	}

	public ArrayList<String> getUrlKeywords() {
		return urlKeywords;
	}

	public ArrayList<String> getPageKeywords() {
		return pageKeywords;
	}
}
