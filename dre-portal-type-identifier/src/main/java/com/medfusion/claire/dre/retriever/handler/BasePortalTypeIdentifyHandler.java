package com.medfusion.claire.dre.retriever.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.medfusion.claire.app.data.DreStatusType;
import com.medfusion.claire.app.data.ticket.DreTaskType;
import com.medfusion.claire.app.data.ticket.DreTicketInfo;
import com.medfusion.claire.app.data.ticket.TaskResultInfo;
import com.medfusion.claire.app.data.ticket.TaskResultType;
import com.medfusion.claire.app.data.ticket.TicketTaskInfo;
import com.medfusion.claire.dre.clients.SeleniumClient;
import com.medfusion.claire.dre.exceptions.NonRecoverableException;
import com.medfusion.claire.dre.retriever.navigator.BaseNavigator;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.data.PortalTypeIdentifier;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;
import com.medfusion.claire.dre.retriever.portaltypeidentifiers.AllScriptsPortalTypeIdentifier;
import com.medfusion.claire.dre.retriever.portaltypeidentifiers.AthenaPortalTypeIdentifier;
import com.medfusion.claire.dre.retriever.portaltypeidentifiers.Epic2017PortalTypeIdentifier;
import com.medfusion.claire.dre.retriever.ticket.CapabilitiesBuilder;
import com.medfusion.claire.dre.utils.TicketUtils;

public class BasePortalTypeIdentifyHandler extends BaseHandler {
	private static final Logger LOG = LoggerFactory.getLogger(BasePortalTypeIdentifyHandler.class);
	private DreTicketInfo ticket;
	private String URL;
	private ArrayList<PortalTypeIdentifier> portalTypes;

	public BasePortalTypeIdentifyHandler(PortalTypeIdentifierNavigator navigator) {
		super(navigator);
		this.ticket = navigator.getTicket();
		this.URL = navigator.getTicket().getPortalInfo().getPrimaryUrl();
		this.portalTypes = new ArrayList<PortalTypeIdentifier>();
		buildPortalTypes();
	}

	@Override
	public <T extends TicketTaskInfo> void handleTask(T task) {

		ArrayList<String> possibleTypes = new ArrayList<String>();
		for (PortalTypeIdentifier portalTypeIdentifier : portalTypes) {
			try {
				portalTypeIdentifier.getNavigator().gotoPage(URL);
				if (portalTypeIdentifier.isCopyrightCorrectType()) {
					addPossibleType(possibleTypes, portalTypeIdentifier.getPortalType());
				} else if (portalTypeIdentifier.isUniquePortalInfoCorrect()) {
					addPossibleType(possibleTypes, portalTypeIdentifier.getPortalType());
				} else if (portalTypeIdentifier.isURLCorrect()) {
					addPossibleType(possibleTypes, portalTypeIdentifier.getPortalType());
				}
			} catch (NonRecoverableException e) {
				throw e;
			} catch (Exception e) {
				LOG.info("Expection throw during Portal Type identification {}", e.getMessage());
			} finally {
				portalTypeIdentifier.close();
			}
		}

		List<String> altInfo = ticket.getPortalInfo().getTypeInfo().getAdditionalInfoFields();
		if (possibleTypes.size() > 0) {
			for (String type : possibleTypes) {
				LOG.info("Portal Type found with type : {}", type);
				altInfo.add("Possilbe Portal Type: " + type);
				TaskResultInfo result = new TaskResultInfo();
				result.setStatus(TaskResultType.UPDATED);
				task.addResult(result);
			}
			ticket.setJobStatus(DreStatusType.SUCCESS);
			ticket.getPortalInfo().getTypeInfo().setAdditionalInfoFields(altInfo);
		}
	}
	
	private synchronized void addPossibleType(ArrayList<String> possibleTypes, String type) {
		possibleTypes.add(type);
	}
	/**
	 * Every new PortalTypeIdentifier must be defined here
	 */
	private void buildPortalTypes() {
		portalTypes.add(new Epic2017PortalTypeIdentifier(initalizeNavigator(new PortalTypeIdentifierNavigator())));
		portalTypes.add(new AllScriptsPortalTypeIdentifier(initalizeNavigator(new PortalTypeIdentifierNavigator())));
		portalTypes.add(new AthenaPortalTypeIdentifier(initalizeNavigator(new PortalTypeIdentifierNavigator())));
	}

	/**
	 * This method ensures that the navigator is initalized correctly.
	 * 
	 * @param navigator
	 * @return
	 */
	private PortalTypeIdentifierNavigator initalizeNavigator(PortalTypeIdentifierNavigator navigator) {
		if (null == ticket) {
			throw new NonRecoverableException("ticket cannot be null");
		}
		navigator.setTicket(ticket);
		navigator.setStrings(this.navigator.getStrings());
		
		if (null == ticket.getTicketId()) {
			throw new NonRecoverableException("ticketId cannot be null");
		}
		if (null == navigator.getTicket().getPortalInfo()) {
			throw new NonRecoverableException("Portal Info cannot be null");
		}
		if (StringUtils.isBlank(navigator.getTicket().getPortalInfo().getPrimaryUrl())) {
			throw new NonRecoverableException("Primary Url cannot be empty");
		}
		TicketUtils.resolveAndCreateTicketFolders(navigator.getTicket());
		final Map<String, String> desiredCapabilitiesMap = navigator.getTicket().getPortalInfo()
				.getSeleniumOptions();
		final CapabilitiesBuilder builder = new CapabilitiesBuilder();
		final DesiredCapabilities desiredCapabilities = builder.build(desiredCapabilitiesMap);

		navigator.getSeleniumClient().initialize(desiredCapabilities, desiredCapabilitiesMap);
		return navigator;
	}

	@Override
	public BaseNavigator getNavigator() {
		return navigator;
	}
	
	
}
