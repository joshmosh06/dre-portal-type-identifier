package com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.medfusion.claire.app.data.DreStatusType;
import com.medfusion.claire.app.data.credential.CredentialInfo;
import com.medfusion.claire.dre.clients.SeleniumClient;
import com.medfusion.claire.dre.exceptions.NonRecoverableException;
import com.medfusion.claire.dre.retriever.download.BaseCcdDownloader;
import com.medfusion.claire.dre.retriever.navigator.BaseNavigator;
import com.medfusion.claire.dre.retriever.test.strings.PortalTypeIdentifierStrings;

public class PortalTypeIdentifierNavigator extends BaseNavigator<CredentialInfo> {
	private static final Logger LOG = LoggerFactory.getLogger(PortalTypeIdentifierNavigator.class);
	private static final String ID = "@id";
	private static final String TYPE = "@type";
	private static final String CLASS = "@class";
	private static final String TEXT = "text()";

	/**
	 * Public ctor.
	 */
	public PortalTypeIdentifierNavigator() {
		setSeleniumClient(new SeleniumClient());
	}

	public PortalTypeIdentifierNavigator(boolean b) {
		setSeleniumClient(null);
	}
	
	public boolean isCopyrightPresent(ArrayList<String> keywords) {
		ArrayList<String> elements = locateElements(keywords);
		return elements != null && elements.size()>0;
	}
	
	/**
	 * Locates the elements for given search keywords
	 * 
	 * @param keywords
	 *            - ArrayList of Strings that may match the id or name of elements
	 *            in a webpage. Ex: "user" "login" "sign in" etc
	 *            
	 * @return returns the String to the xpath of a new located element returns null
	 */
	private ArrayList<String> locateElements(ArrayList<String> keywords) {
		ArrayList<String> elements = new ArrayList<String>();
		String type = "//*";
		// returns all elements for evaluation.
		List<WebElement> allElements = getSeleniumClient().getElementsIfPresent(By.xpath(type));
		if (String.valueOf(allElements).equals("null") || allElements.size() == 0) {
			// The xpath type is not found on the screen, so lets try all types just in case
			allElements = getSeleniumClient().getElementsIfPresent(By.xpath(type));
		}
		for (WebElement webElement : allElements) {
			for (String keyword : keywords) {
				keyword = keyword.toLowerCase();
				String newPath = null;

				if (String.valueOf(webElement.getAttribute("id")).toLowerCase().contains(keyword)) {
					newPath = createXpath(type, ID, webElement.getAttribute("id"));
				} else if (String.valueOf(webElement.getAttribute("class")).toLowerCase().contains(keyword)) {
					newPath = createXpath(type, CLASS, webElement.getAttribute("class"));
				} else if (String.valueOf(webElement.getAttribute("type")).toLowerCase().contains(keyword)) {
					newPath = createXpath(type, TYPE, webElement.getAttribute("type"));
				} else if (String.valueOf(webElement.getText()).contains(keyword)) {
					newPath = createXpath(type, TEXT, keyword);
				} else if (String.valueOf(webElement.getText()).toLowerCase().contains(keyword)) {
					newPath = findSubElementWithText(keyword, webElement);
				}
				if (newPath != null) {
					elements.add(newPath);
				}
			}
		}
		if(elements.isEmpty()) {
			return null;
		}
		return elements;
	}
	
	/**
	 * Determines the most descendant element containing the specified keyword and
	 * type of a provided web element
	 * 
	 * @param keyword
	 *            - keyword of element that was found
	 * @param type
	 *            - type of element looking for
	 * @param webElement
	 *            - the web element that was found with text containing the keyword.
	 * @return - the xpath to the most descendant element containing the keyword
	 */
	protected String findSubElementWithText(String keyword, WebElement webElement) {
		List<WebElement> subElements = webElement.findElements(By.xpath(".//*"));
		for (int i = subElements.size() - 1; i >= 0; i--) {
			WebElement subElement = subElements.get(i);
			if (String.valueOf(subElement.getText().toLowerCase()).contains(keyword)) {
				int keywordPos = subElement.getText().toLowerCase().indexOf(keyword);
				return createXpath("//*", TEXT,
						subElement.getText().substring(keywordPos, keywordPos + keyword.length()));
			}
		}
		return null;
	}
	
	/**
	 * @param type
	 *            - type of element such as button input or * for all
	 * @param expression
	 *            - expression within xpath used to locate and uniquely identify a
	 *            element
	 * @param attribute
	 *            - value of the attribute used
	 * @return - returns the full xpath
	 */
	protected String createXpath(String type, String expression, String attribute) {
		if (expression == null || attribute == null) {
			return null;
		}
		if (type.contains("|") || String.valueOf(type).equals("null") || String.valueOf(type).equals("")) {
			type = "//*";
		}
		if (expression.equals(TEXT)) {
			return type + "[contains(text(),'" + attribute + "')]";
		}

		return type + "[" + expression + "='" + attribute + "']";
	}

	@Override
	public PortalTypeIdentifierStrings getStrings() {
		return (PortalTypeIdentifierStrings) this.strings;
	}

	@Override
	public BaseCcdDownloader getDownloader() {
		// ccd downloader not used for this retriever
		return null;
	}

	@Override
	public Pair<DreStatusType, String> login(CredentialInfo credentials) {return null;}

	@Override
	public void logout() {
		LOG.debug("logout() - called with ticketId={}",
				null == this.getTicket() ? null : this.getTicket().getTicketId());
		// no-op
		return;
	}
	@Override
	public void goHome() {}

	public boolean isURLCorrect(ArrayList<String> urlKeywords) {
		String url = getTicket().getPortalInfo().getPrimaryUrl();
		for (String keyword : urlKeywords) {
			if(url.contains(keyword)) {
				return true;
			}
		}
		return false;
	}

	public void gotoPage(String url) {
		try {
			goToLoginPage(url);
		} catch(Exception e) {
			getTicket().setJobStatus(DreStatusType.ERROR_OTHER);
			getTicket().setJobStatusMessage("Unable to Navagate to page" + url + " \n " + e.getMessage());
			throw new NonRecoverableException("Unable to Navagate to page " + url, e.getCause());
		}
		
	}

}
