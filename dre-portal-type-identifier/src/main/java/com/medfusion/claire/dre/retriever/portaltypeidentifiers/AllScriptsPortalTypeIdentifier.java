package com.medfusion.claire.dre.retriever.portaltypeidentifiers;

import java.util.ArrayList;
import java.util.Arrays;

import com.medfusion.claire.dre.retriever.portaltypeidentifier.data.PortalTypeIdentifier;
import com.medfusion.claire.dre.retriever.portaltypeidentifier.navigator.PortalTypeIdentifierNavigator;

public class AllScriptsPortalTypeIdentifier extends PortalTypeIdentifier  {
	
	private static final ArrayList<String> ALLSCRIPTS_PAGE_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "allscripts", "followmyhealth" }));

	private static final ArrayList<String> ALLSCRIPTS_URL_KEYWORDS = new ArrayList<String>(
			Arrays.asList(new String[] { "followmyhealth" }));
	
	public AllScriptsPortalTypeIdentifier(PortalTypeIdentifierNavigator navigator) {
		super(navigator, ALLSCRIPTS_PAGE_KEYWORDS, ALLSCRIPTS_URL_KEYWORDS);
	}

	@Override
	public String getPortalType() {
		return "allscripts";
	}
}